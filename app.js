var express = require('express');
var app = express();

var path = require('path');


// configure our app to handle CORS requests
app.use(function (req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header(
		"Access-Control-Allow-Headers",
		"Origin, X-Requested-With, Content-Type, Accept, Authorization, Access-Control-Allow-Credentials, Access-Control-Allow-Origin"
	);
	res.header(
		"Access-Control-Allow-Methods",
		"PUT, GET, POST, DELETE"
	);
	res.header("Access-Control-Allow-Credentials", "true");
	next();
});






//======================================================Connect to Mongoose================================================================




    
app.use(express.static(path.join(__dirname, 'public')));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'html');
//=======================================================HTML Pages=====================================================
app.get('/', function (req, res) {
    res.sendFile(__dirname + '/views/index.html');
});



//=======================================================Routs============================================================================




//=========================================================================================================================================
const PORT = process.env.PORT || 3000;
app.listen(PORT);
console.log("Code Way Server connected to port" + " " + PORT);